package cs601.concurrent;


import java.util.Map;
import java.util.TreeMap;

/**
 * A reentrant read/write lock that allows: 
 * 1) Multiple readers (when there is no writer).
 * 2) One writer (when nobody else is writing or reading). 
 * 3) A writer is allowed to acquire a read lock while holding the write lock. 
 * The assignment is based on the assignment of Prof. Rollins (original author).
 */
public class ReentrantReadWriteLock {

	// TODO: Add instance variables : you need to keep track of the read lock holders and the write lock holders.
	// We should be able to find the number of read locks and the number of write locks 
	// a thread with the given threadId is holding

	private static int readers = 0;
	private static int writers = 0;

	private final Object readLock;
	private final Object writeLock;

	private final Map <Long, Integer> readLockMap = new TreeMap<>();
	private final Map <Long, Integer> writeLockMap = new TreeMap<>();

	private int numOfReadLock;
	private int numOfWriteLock;
	
	/**
	 * Constructor for ReentrantReadWriteLock
	 */
	public ReentrantReadWriteLock() {
		// FILL IN CODE

		readLock = new Object();
		writeLock = new Object();
		numOfReadLock = 0;
		numOfWriteLock = 0;
	}

	/**
	 * Returns true if the current thread holds a read lock.
	 * 
	 * @return
	 */
	public synchronized boolean isReadLockHeldByCurrentThread() {

		// FILL IN CODE

		if (readLockMap.get(Thread.currentThread().getId()) > 0){
			return true;
		}
		return false; // don't forget to change it
	}

	/**
	 * Returns true if the current thread holds a write lock.
	 * 
	 * @return
	 */
	public synchronized boolean isWriteLockHeldByCurrentThread() {
		// FILL IN CODE

		if (writeLockMap.get(Thread.currentThread().getId()) > 0){
			return true;
		}
		return false; // don't forget to change it
	}

	/**
	 * Non-blocking method that tries to acquire the read lock. Returns true
	 * if successful.
	 * 
	 * @return
	 */
	public synchronized boolean tryAcquiringReadLock() {
		// FILL IN CODE

		if (writers > 0){

			// if the writer is the thread its own, it is still allowed to acquire read lock

			if (writeLockMap.get(Thread.currentThread().getId()) == writers){
				return true;
			}
			return false;
		}

		return true; // don't forget to change it
	}

	/**
	 * Non-blocking method that tries to acquire the write lock. Returns true
	 * if successful.
	 * 
	 * @return
	 */
	public synchronized boolean tryAcquiringWriteLock() {
		// FILL IN CODE

		if (writers > 0 || readers > 0){
			try {
				if (readLockMap.get(Thread.currentThread().getId()) > 0){
					return false;
				}
				// the thread are allowed to reacquire write lock again if there is no other writers and readers
				else if (writeLockMap.get(Thread.currentThread().getId()) == writers){
					return true;
				}else{
					return false;
				}

			}catch (NullPointerException e){
				e.printStackTrace();
			}

		}

		
		return true; // don't forget to change it
	}

	/**
	 * Blocking method - calls tryAcquiringReadLock and returns only when the read lock has been
	 * acquired, otherwise waits.
	 * 
	 * @throws InterruptedException
	 */
	public synchronized void lockRead() {
		// FILL IN CODE

		try {
			while (!tryAcquiringReadLock()){
				this.wait();
			}
			synchronized (readLock){
				this.readers += 1;
				this.numOfReadLock += 1;

//				Thread.holdsLock(readLock);
				readLockMap.put(Thread.currentThread().getId(), this.numOfReadLock);
			}
		}catch (InterruptedException ex){
			ex.printStackTrace();
		}

	}

	/**
	 * Releases the read lock held by the current thread. 
	 */
	public synchronized void unlockRead() {
		// FILL IN CODE

		notifyAll();
		this.readers -= 1;
		this.numOfReadLock -= 1;

		readLockMap.put(Thread.currentThread().getId(), this.numOfReadLock);
	}

	/**
	 * Blocking method that calls tryAcquiringWriteLock and returns only when the write lock has been
	 * acquired, otherwise waits.
	 * 
	 * @throws InterruptedException
	 */
	public synchronized void lockWrite() {
		// FILL IN CODE

		try {
			while (!tryAcquiringWriteLock()){
				this.wait();
			}
			synchronized (writeLock){
				this.writers += 1;
				this.numOfWriteLock += 1;

//				Thread.holdsLock(writeLock);
				writeLockMap.put(Thread.currentThread().getId(), this.numOfWriteLock);
			}
		}catch (InterruptedException ex){
			ex.printStackTrace();
		}

	}

	/**
	 * Releases the write lock held by the current thread. 
	 */

	public synchronized void unlockWrite() {
		// FILL IN CODE

		notifyAll();
		this.writers -= 1;
		this.numOfWriteLock -= 1;

		writeLockMap.put(Thread.currentThread().getId(), this.numOfWriteLock);
	}
}
