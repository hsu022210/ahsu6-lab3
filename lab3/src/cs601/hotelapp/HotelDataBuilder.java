package cs601.hotelapp;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created by Alec on 9/28/2016.
 *
 * Class HotelDataBuilder - a class contains loadHotelInfo and loadReviews mehtods.
 */
public class HotelDataBuilder {
    private ThreadSafeHotelData data;

    /**
     * Default constructor.
     */
    public HotelDataBuilder(){
        data = new ThreadSafeHotelData();
    }


    /**
     * Read the json file with information about the hotels (id, name, address,
     * etc) and load it into the appropriate data structure(s). Note: This
     * method does not load reviews
     *
     * @param filename
     *            the name of the json file that contains information about the
     *            hotels
     */
    public void loadHotelInfo(String jsonFilename) {

        // Hint: Use JSONParser from JSONSimple library
        // FILL IN CODE

        //call addHotels method based on jspn files

        JSONParser parser = new JSONParser();

        try {

            Object obj = parser.parse(new FileReader(jsonFilename));

            JSONObject jsonObject = (JSONObject) obj;

            JSONArray hotelResults = (JSONArray) jsonObject.get("sr");

            for (int i=0; i < hotelResults.size(); i++){

                JSONObject singleHotel = (JSONObject) hotelResults.get(i);

                String hotelId = (String) singleHotel.get("id");
                String hotelName = (String) singleHotel.get("f");
                String city = (String) singleHotel.get("ci");
                String state = (String) singleHotel.get("pr");
                String streetAddress = (String) singleHotel.get("ad");

                JSONObject latAndLon = (JSONObject) singleHotel.get("ll");

                String lat = (String) latAndLon.get("lat");
                String lon = (String) latAndLon.get("lng");

                double latTypeDouble = Double.parseDouble(lat);
                double lonTypeDouble = Double.parseDouble(lon);

                data.addHotel(hotelId, hotelName, city, state, streetAddress, latTypeDouble, lonTypeDouble);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    /**
     * Load reviews for all the hotels into the appropriate data structure(s).
     * Traverse a given directory recursively to find all the json files with
     * reviews and load reviews from each json. Note: this method must be
     * recursive and use DirectoryStream as discussed in class.
     *
     * @param path
     *            the path to the directory that contains json files with
     *            reviews Note that the directory can contain json files, as
     *            well as subfolders (of subfolders etc..) with more json files
     */
    public void loadReviews(Path path) {
        // FILL IN CODE

        // Hint: first, write a separate method to read a single json file with
        // reviews
        // using JSONSimple library
        // Call this method from this one as you traverse directories and find
        // json files

        try {
            DirectoryStream<Path> pathsList = Files.newDirectoryStream(path);

            for (Path each_path : pathsList) {

                if (Files.isDirectory(each_path)){

                    loadReviews(each_path);

                }else{

                    data.parseSingleJsonFile(each_path.toString());

                }
            }
        } catch (IOException e) {
            System.out.println("IOException occurred");
            e.printStackTrace();

        }

    }
}
