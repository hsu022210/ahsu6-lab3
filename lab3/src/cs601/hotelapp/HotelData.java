package cs601.hotelapp;

import java.io.*;
import java.nio.file.Path;
import java.util.*;

import java.nio.file.Files;
import java.nio.file.DirectoryStream;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import static java.nio.file.StandardOpenOption.*;


/**
 * Class HotelData - a data structure that stores information about hotels and
 * hotel reviews. Allows to quickly lookup a hotel given the hotel id.
 * Allows to easily find hotel reviews for a given hotel, given the hotelID.
 * Reviews for a given hotel id are sorted by the date and user nickname.
 *
 */
public class HotelData {

	// FILL IN CODE - declare data structures to store hotel data
	private TreeMap hotelInfoMap;
	private TreeMap hotelReviewMap;
	private TreeSet reviewTreeSet;

	/**
	 * Default constructor.
	 */
	 
	 //TODO: just initialize instance variables here, instead of creating new data structures
	public HotelData() {
		// Initialize all data structures
		// FILL IN CODE

		this.hotelInfoMap = new TreeMap<String, Hotel>();
		this.hotelReviewMap = new TreeMap<String, TreeSet>();
		this.reviewTreeSet = new TreeSet<Review>();

	}

	/**
	 * Create a Hotel given the parameters, and add it to the appropriate data
	 * structure(s).
	 *
	 * @param hotelId
	 *            - the id of the hotel
	 * @param hotelName
	 *            - the name of the hotel
	 * @param city
	 *            - the city where the hotel is located
	 * @param state
	 *            - the state where the hotel is located.
	 * @param streetAddress
	 *            - the building number and the street
	 * @param latitude
	 * @param longitude
	 */
	public void addHotel(String hotelId, String hotelName, String city, String state, String streetAddress, double lat,
			double lon) {
		// FILL IN CODE

		Hotel hotel = new Hotel(hotelId, hotelName, city, state, streetAddress, lat, lon);
		this.hotelInfoMap.put(hotelId, hotel);

	}

	/**
	 * Add a new review.
	 *
	 * @param hotelId
	 *            - the id of the hotel reviewed
	 * @param reviewId
	 *            - the id of the review
	 * @param rating
	 *            - integer rating 1-5.
	 * @param reviewTitle
	 *            - the title of the review
	 * @param review
	 *            - text of the review
	 * @param isRecommended
	 *            - whether the user recommends it or not
	 * @param date
	 *            - date of the review in the format yyyy-MM-dd, e.g.
	 *            2016-08-29.
	 * @param username
	 *            - the nickname of the user writing the review.
	 * @return true if successful, false if unsuccessful because of invalid date
	 *         or rating. Needs to catch and handle ParseException if the date is invalid.
	 *         Needs to check whether the rating is in the correct range
	 */
	public boolean addReview(String hotelId, String reviewId, int rating, String reviewTitle, String review,
			boolean isRecom, String date, String username) {

		// FILL IN CODE

		Review singleReview;

//		if (getHotels().contains(hotelId) == false){
//			return false;
//		}

		if(date.matches("\\d{4}-\\d{2}-\\d{2}") == false){
			return false;
		}

		if (rating<=5 && rating>=1){
			singleReview = new Review(hotelId, reviewId, rating, reviewTitle, review, isRecom, date, username);

			this.reviewTreeSet.add(singleReview);

			TreeSet temp = (TreeSet) this.reviewTreeSet.clone();

			this.hotelReviewMap.put(hotelId, temp);

			return true;
		}

		return false; // don't forget to change it
	}


	/**
	 * Return an alphabetized list of the ids of all hotels
	 *
	 * @return
	 */
	public List<String> getHotels() {
		// FILL IN CODE

		List<String> idList = new ArrayList<String>(this.hotelInfoMap.keySet());

		return idList; // don't forget to change it
	}

	/**
	 * Read the json file with information about the hotels (id, name, address,
	 * etc) and load it into the appropriate data structure(s). Note: This
	 * method does not load reviews
	 *
	 * @param filename
	 *            the name of the json file that contains information about the
	 *            hotels
	 */
	public void loadHotelInfo(String jsonFilename) {

		// Hint: Use JSONParser from JSONSimple library
		// FILL IN CODE

		//call addHotels method based on jspn files

		JSONParser parser = new JSONParser();

		try {

			Object obj = parser.parse(new FileReader(jsonFilename));

			JSONObject jsonObject = (JSONObject) obj;

			JSONArray hotelResults = (JSONArray) jsonObject.get("sr");

			for (int i=0; i < hotelResults.size(); i++){

				JSONObject singleHotel = (JSONObject) hotelResults.get(i);

				String hotelId = (String) singleHotel.get("id");
				String hotelName = (String) singleHotel.get("f");
				String city = (String) singleHotel.get("ci");
				String state = (String) singleHotel.get("pr");
				String streetAddress = (String) singleHotel.get("ad");

				JSONObject latAndLon = (JSONObject) singleHotel.get("ll");

				String lat = (String) latAndLon.get("lat");
				String lon = (String) latAndLon.get("lng");

				double latTypeDouble = Double.parseDouble(lat);
				double lonTypeDouble = Double.parseDouble(lon);

				addHotel(hotelId, hotelName, city, state, streetAddress, latTypeDouble, lonTypeDouble);

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}


	public boolean getBooleanForIsRecom(String isRecomString){
		if (isRecomString == "YES"){
			return true;
		}else {
			return false;
		}
	}


	public void parseSingleJsonFile(String jsonFilename){

		JSONParser parser = new JSONParser();

		try {

			Object obj = parser.parse(new FileReader(jsonFilename));

			JSONObject jsonObject = (JSONObject) obj;

			JSONObject reviewDetails = (JSONObject) jsonObject.get("reviewDetails");
			JSONObject reviewCollection = (JSONObject) reviewDetails.get("reviewCollection");
			JSONArray reviewList = (JSONArray) reviewCollection.get("review");

			for (int i=0; i < reviewList.size(); i++){

				JSONObject singleReview = (JSONObject) reviewList.get(i);

				String hotelId = (String) singleReview.get("hotelId");

				String reviewId = (String) singleReview.get("reviewId");

				long ratingLong = (long) singleReview.get("ratingOverall");
				int rating = (int) ratingLong;

				String reviewTitle = (String) singleReview.get("title");
				String review = (String) singleReview.get("reviewText");

				String isRecomString = (String) singleReview.get("isRecommended");
				boolean isRecom = getBooleanForIsRecom(isRecomString);

				String rawDate = (String) singleReview.get("reviewSubmissionTime");
				String[] dateSplitList = rawDate.split("T");
				String date = dateSplitList[0];

				String username = (String) singleReview.get("userNickname");

				if (username.equals("")){
					username = "anonymous";
				}

				boolean addReviewResult = addReview(hotelId, reviewId, rating, reviewTitle, review, isRecom, date, username);

			}

			this.reviewTreeSet.clear();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}


	/**
	 * Load reviews for all the hotels into the appropriate data structure(s).
	 * Traverse a given directory recursively to find all the json files with
	 * reviews and load reviews from each json. Note: this method must be
	 * recursive and use DirectoryStream as discussed in class.
	 *
	 * @param path
	 *            the path to the directory that contains json files with
	 *            reviews Note that the directory can contain json files, as
	 *            well as subfolders (of subfolders etc..) with more json files
	 */
	public void loadReviews(Path path) {
		// FILL IN CODE

		// Hint: first, write a separate method to read a single json file with
		// reviews
		// using JSONSimple library
		// Call this method from this one as you traverse directories and find
		// json files

		try {
			DirectoryStream<Path> pathsList = Files.newDirectoryStream(path);

			for (Path each_path : pathsList) {

				if (Files.isDirectory(each_path)){

					loadReviews(each_path);

				}else{

					parseSingleJsonFile(each_path.toString());

				}
			}
		} catch (IOException e) {
			System.out.println("IOException occurred");
			e.printStackTrace();

		}

	}

	/**
	 * Returns a string representing information about the hotel with the given
	 * id, including all the reviews for this hotel separated by
	 * -------------------- Format of the string: HoteName: hotelId
	 * streetAddress city, state -------------------- Review by username: rating
	 * ReviewTitle ReviewText -------------------- Review by username: rating
	 * ReviewTitle ReviewText ...
	 *
	 * @param hotel
	 *            id
	 * @return - output string.
	 */
	public String toString(String hotelId) {

		// FILL IN CODE

		if (getHotels().contains(hotelId) == false){
			return "";
		}

		Hotel hotelInfo = (Hotel) this.hotelInfoMap.get(hotelId);

		TreeSet<Review> hotelReview = (TreeSet<Review>) this.hotelReviewMap.get(hotelId);

		String result;
		String newLine = "\n";

		Address hotelAddress = hotelInfo.getAddress();

		result = hotelInfo.getHotelName() + ": " + hotelInfo.getHotelId();
		result += newLine;

		result += hotelAddress.getStreetAddress();
		result += newLine;

		result += hotelAddress.getCity() + ", " + hotelAddress.getState();
		result += newLine;

		try {

			for (Review r : hotelReview){

				result += "--------------------";
				result += newLine;

				String userName = r.getUsername();

				result += "Review by " + userName + ": " + r.getRating();
				result += newLine;

				result += r.getReviewTitle();
				result += newLine;

				result += r.getReview();
				result += newLine;

			}

		}catch (NullPointerException e){
			e.printStackTrace();
		}finally {
			return result;
		}

//		return result; // don't forget to change to the correct string
	}

	/**
	 * Save the string representation of the hotel data to the file specified by
	 * filename in the following format:
	 * an empty line
	 * A line of 20 asterisks ******************** on the next line
	 * information for each hotel, printed in the format described in the toString method of this class.
	 *
	 * @param filename
	 *            - Path specifying where to save the output.
	 */
	public void printToFile(Path filename) {
		// FILL IN CODE

		List<String> hotelIdList = getHotels();

		try (OutputStream out = new BufferedOutputStream(
				Files.newOutputStream(filename, CREATE, APPEND))) {

			for (String id: hotelIdList){
				String result = "\n" + "********************" + "\n";
				result += toString(id);
				byte data[] = result.getBytes();
				out.write(data, 0, data.length);
			}

		} catch (IOException x) {
			System.err.println(x);
		}
	}
}
