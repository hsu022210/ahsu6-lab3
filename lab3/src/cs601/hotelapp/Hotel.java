package cs601.hotelapp;

import java.nio.file.Path;
import java.util.*;


public class Hotel implements Comparable<Hotel>{

    private String hotelId;
    private String hotelName;
    private Address address;

    public Hotel(String hotelId, String hotelName, String city, String state, String streetAddress, double lat,
			double lon){
                this.hotelId = hotelId;
                this.hotelName = hotelName;
                this.address = new Address(city, state, streetAddress, lat, lon);
    }


    @Override
	public int compareTo(Hotel h) {

		int dif = hotelName.compareTo(h.hotelName);

        return dif;
	}


//TODO: need getter or setter methods


    public String getHotelId(){
        return hotelId;
    }

    public void setHotelId(String hotelId){
        this.hotelId = hotelId;
    }


    public String getHotelName(){
        return hotelName;
    }

    public void setHotelName(String hotelName){
        this.hotelName = hotelName;
    }


    public Address getAddress(){
        return address;
    }

    public void setAddress(String city, String state, String streetAddress, double lat,
                           double lon){
        this.address = new Address(city, state, streetAddress, lat, lon);
    }

}
